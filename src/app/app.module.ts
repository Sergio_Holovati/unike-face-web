import { RegisterComponent } from './module/page/register/component/register.component';
import { SendDocumentComponent } from './module/page/send-document/component/send-document.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {WebcamModule} from 'ngx-webcam';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from "@angular/common";
import { AppComponent } from './app.component';
import { LoginComponent } from './module/page/access/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {TokenInterceptor} from './module/auth/token.interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TakescreenshotComponent } from './module/page/screenshot/takescreenshot/takescreenshot.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SendDocumentComponent,
    RegisterComponent,
    TakescreenshotComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,  
    ReactiveFormsModule,
    MatSnackBarModule,
    WebcamModule,    
    BrowserAnimationsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
