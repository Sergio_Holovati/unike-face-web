import { SendDocumentComponent } from './../send-document/component/send-document.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
 
    {
        path: 'send-document',
        component: SendDocumentComponent
    }
    
];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class AccessRoutingModule{}