import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-send-document',
  templateUrl: './send-document.component.html',
  styleUrls: [
    './send-document.component.css',
    '../../../../css/bootstrap.min.css',
    '../../../../css/fontawesome-all.min.css',
    '../../../../css/unike-face-theme.css',
  ],
})
export class SendDocumentComponent implements OnInit {
  public formDocument!: FormGroup;
  constructor(private fbs: FormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.documetForm();
  }

  documetForm() {
    this.formDocument = this.fbs.group({
      documentNumber: ['', Validators.compose([Validators.required])]
    });
  }

  getDocument() {
    let document = this.formDocument.value ;
    console.log(document.documentNumber);
    if( document.documentNumber == '12345678'){
      this.router.navigate(['/home']);
    }
    else{
      this.router.navigate(['/register']);
    }
   /*  this.login.LoginUser(this.signinForm.value).subscribe(   
      resultado => {                 
         localStorage.setItem('token',resultado['token'])      
         this.router.navigate(['/home']); 
         console.log('deu certo');
       },
       erro => {
         if(erro.status) {
            this.login.showMessage('Usuário Não Localizado!');
            localStorage.clear();
         }
       });  */
     }


}
