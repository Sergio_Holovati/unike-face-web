import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';

import {Header} from '../../../../auth/Header.model';

import { Router} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn:'root'
})

export class RegisterService {

    heade!:Header;

    constructor(private http: HttpClient , private router : Router,private snackBar : MatSnackBar){}
    baseUrl = '';


    showMessage(msg: string, isError: boolean = false): void {
        this.snackBar.open(msg, "X", {
          duration: 3000,
          horizontalPosition: "left",
          verticalPosition: "top",
          panelClass: isError ? ["msg-error"] : ["msg-success"],
        });
      }


  
}