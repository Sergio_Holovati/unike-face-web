import { RegisterService } from './register-model/register.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css','../../../../css/bootstrap.min.css','../../../../css/fontawesome-all.min.css','../../../../css/unike-face-theme.css']
})
export class RegisterComponent implements OnInit {

  
  public cadastroForm!: FormGroup;
  constructor(private fbs: FormBuilder, private router: Router, private registerService : RegisterService) {}

  ngOnInit(): void {
    this.cadastroFormFuncion();
  }
  
  cadastroFormFuncion() {
    this.cadastroForm = this.fbs.group({
        email: [
            '',
            Validators.compose([

                Validators.required,
            ]),
        ],
        name: [
            '',
            Validators.compose([

                Validators.required,
            ]),
        ],
    });
    
}
  cadastrarFace(){
    let document = this.cadastroForm.value ;
    if( document.name != '' && document.email != ''){
      this.router.navigate(['/takescreenshot']);
    }
    else{
      this.registerService.showMessage("Insira todos os campos")
    }
  }

}
