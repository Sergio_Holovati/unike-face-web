
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { AccessRoutingModule } from './access-routing.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
    declarations:[
        LoginComponent,
        ForgotPasswordComponent
    ],
    imports:[
        CommonModule,
        AccessRoutingModule
    ],
    providers:[{
        provide:MAT_RADIO_DEFAULT_OPTIONS,
        useValue:{color:'accent'}
    }]
})

export class AccessModule{}

