import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login-models/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css',
    '../../../../css/bootstrap.min.css',
    '../../../../css/fontawesome-all.min.css',
    '../../../../css/unike-face-theme.css',
  ],
})
export class LoginComponent implements OnInit {
    

  public signinForm! : FormGroup;

  constructor(private fbs: FormBuilder,private router: Router,private login : LoginService) {}
  

  ngOnInit(): void {
   this.signinUserForm();
  } 

  signinUserForm() {
    this.signinForm = this.fbs.group({
        email: [
            '',
            Validators.compose([

                Validators.required,
            ]),
        ],
        senha: [
            '',
            Validators.compose([

                Validators.required,
            ]),
        ],
    });
    
}

  loginUsuario() {
    this.router.navigate(['/send-document']); 
   /*  this.login.LoginUser(this.signinForm.value).subscribe(   
      resultado => {                 
         localStorage.setItem('token',resultado['token'])      
         this.router.navigate(['/home']); 
         console.log('deu certo');
       },
       erro => {
         if(erro.status) {
            this.login.showMessage('Usuário Não Localizado!');
            localStorage.clear();
         }
       });  */
     }
}
