import { SendDocumentComponent } from './../send-document/component/send-document.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'send-document',
        component: SendDocumentComponent
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent
    }
    
];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class AccessRoutingModule{}