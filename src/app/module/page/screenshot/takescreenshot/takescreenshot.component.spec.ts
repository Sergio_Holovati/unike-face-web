import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakescreenshotComponent } from './takescreenshot.component';

describe('TakescreenshotComponent', () => {
  let component: TakescreenshotComponent;
  let fixture: ComponentFixture<TakescreenshotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakescreenshotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakescreenshotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
