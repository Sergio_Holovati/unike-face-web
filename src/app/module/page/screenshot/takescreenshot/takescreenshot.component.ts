import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-takescreenshot',
  templateUrl: './takescreenshot.component.html',
  styleUrls: ['./takescreenshot.component.css','../../../../css/bootstrap.min.css','../../../../css/fontawesome-all.min.css','../../../../css/unike-face-theme.css']
})
export class TakescreenshotComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  sendDocuemt(){
    this.router.navigate(['/send-document']);
  }
}
