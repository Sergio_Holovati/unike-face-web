import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthService {
  public getToken(): any {
    
    return localStorage.getItem('token');
  }


 

  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return tokenNotExpired(null, token);
  }

}

function tokenNotExpired(arg0: null, token: any): boolean {
  throw new Error('Function not implemented.');
}
