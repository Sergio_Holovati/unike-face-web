import { TakescreenshotComponent } from './module/page/screenshot/takescreenshot/takescreenshot.component';
import { ContactComponent } from './module/page/contact/component/contact.component';
import { RegisterComponent } from './module/page/register/component/register.component';
import { SendDocumentComponent } from './module/page/send-document/component/send-document.component';
import { HomeComponent } from './module/page/home/component/home.component';
import { ForgotPasswordComponent } from './module/page/access/forgot-password/forgot-password.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './module/page/access/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'send-document',
    component: SendDocumentComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path:'register',
    component:RegisterComponent
  },{
    path:'takescreenshot',
    component:TakescreenshotComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
